/**
 * 
 */
package test;

import org.junit.jupiter.api.Test;

import data.TransactionHistory;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.junit.Assert;

/**
 * When run, this test will overwrite any saved Transactions, do not run on production systems.
 * @author Andy
 *
 */
public class TransactionHistoryTest {
	
	private static final String testStr1 = "UserID: testUserId TransactionID: 1.0 Amount: 100.0";
	private static final String testStr2 = "UserID: testUserId TransactionID: 2.0 Amount: 3.0";

	/**
	 * sets up transactions for comparison
	 */
	private void setUpTransactions() {
		try {
			Files.delete(Paths.get("StoredTransactions.txt"));
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		TransactionHistory.saveTransaction("testUserId", 1d, 100d);
		TransactionHistory.saveTransaction("testUserId", 2d, 3d);
	}
	
	@Test
	public void testTransactionLoad() {
		setUpTransactions();
		String history = TransactionHistory.readTransactions();
		System.out.println(history);
		Assert.assertTrue(history.contains(testStr1));
		Assert.assertTrue(history.contains(testStr2));
	}
}
