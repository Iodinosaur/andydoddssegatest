/**
 * 
 */
package data;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.LinkOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.nio.file.attribute.FileAttribute;
import java.util.List;


/**
 * Store for all historic Transaction data.
 * 
 * This instantly saves to a file and reads from the file whenever a read is required.
 * This isn't ideal for a high density of calls, but does preserve data through a restart.
 * If needing to handle high traffic, this should cache the transactions and save less frequently.
 * @author Andy
 *
 */
public class TransactionHistory {
	// The file path in which to save the transactions
	private static Path file = Paths.get("StoredTransactions.txt");
	
	/**
	 * Saves a transaction to the file
	 * Will create a file if one doesn't exist
	 * @param userId
	 * @param transactionId
	 * @param amount
	 */
	public static void saveTransaction(String userId, Double transactionId, Double amount) {
		try {
			if (!Files.exists(file, LinkOption.NOFOLLOW_LINKS)) {
				Files.createFile(file, new FileAttribute[0]);
			}
			Transaction newTransaction = new Transaction(userId, transactionId, amount);
			String transStr = newTransaction.toString();
			Files.write(file, transStr.getBytes(), StandardOpenOption.APPEND);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Reads all transactions from the file and returns them
	 * @return
	 */
	public static String readTransactions() {
		StringBuilder sb = new StringBuilder();
		try {
			List<String> fullData = Files.readAllLines(file);
			for (String transStr : fullData) {
				Transaction historicTransaction = new Transaction(transStr);
				sb.append("UserID: " + historicTransaction.getUserId() + 
						" TransactionID: " + historicTransaction.getTransactionId() + 
						" Amount: " + historicTransaction.getAmount() + "\n");
			}
		} catch (Exception e) {
			sb.append(e.getMessage());
		}
		return sb.toString();
		
	}
	
}
