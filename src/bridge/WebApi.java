package bridge;

import static spark.Spark.get;
import static spark.Spark.post;

import data.TransactionHistory;
import spark.Request;
import spark.Response;
import spark.Route;

/**
 * Provides a front facing REST API to save Transactions and get all saved Transactions
 * @author Andy
 *
 */

public class WebApi {

	public static void main(String[] args) {
		//Gets all transactions
		get("/gameTransaction/getAllTransactions", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				return TransactionHistory.readTransactions();
			}
		});
		//Saves a new transaction
		post("/gameTransaction/saveTransaction", new Route() {
			@Override
			public Object handle(Request request, Response response) throws Exception {
				String userId = request.params("userId");
				Double transactionId = Double.valueOf(request.params("transactionId"));
				Double amount = Double.valueOf(request.params("amount"));
				try {
					TransactionHistory.saveTransaction(userId, transactionId, amount);
					return "Transaction Saved.";
				}
				catch (Exception e) {
					return e.getMessage();
				}
			}
		});
	}	
}
